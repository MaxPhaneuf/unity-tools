﻿using ProGrids;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/// <summary>
/// Path created by PathEditor tool to be used by PathFollow tool.
/// </summary>
public class Path : MonoBehaviour
{
    const string WAYPOINT_NAME = "_WayPoint";
    [Tooltip("Will target change direction at the end of path?")]
    public bool isLooping = true;
    [Tooltip("Checking this box will sync all waypoints to the total duration selected below")]
    public bool useTotalDuration;
    [Tooltip("Total duration of the path")]
    [Range(Mathl.MIN_DURATION, Mathl.MAX_DURATION)]public float totalDuration = 1;

    [HideInInspector]
    public WayPoint currentWp, current, next, startPoint;
    [HideInInspector]
    public List<WayPoint> wayPoints = new List<WayPoint>();

    private void Start()
    {
        if (useTotalDuration)
            SyncAtDuration();
    }

    /// <summary>
    /// Sync all waypoints to path total duration.
    /// </summary>
    public void SyncAtDuration()
    {
        wayPoints.RemoveAll(WayPoint => WayPoint == null);
        bool isCycle = IsCycle(wayPoints[0], wayPoints[0]);
        float count = (isCycle ? wayPoints.Count : wayPoints.Count - 1);
        foreach (WayPoint wp in wayPoints)
            count = (wp.bezierTurn.isBezier ? count + 1 : count);
        float d = totalDuration / count;
        foreach (WayPoint wp in wayPoints)
        {
            if (wp.bezierTurn.isBezier)
            {
                wp.bezierTurn.bezierDuration = d;
                wp.lerpToNext.duration = d;
            }
            else
                wp.lerpToNext.duration = d;
        }
    }

    /// <summary>
    /// Adds a new waypoint to this path.
    /// </summary>
    public void NewWayPoint(Vector3 position, float snapValue, bool isLinkedToFirst, Mathl.LerpCurve curveToNext)
    {
        //Progrid dependency
        position = pg_Util.SnapValue(position, snapValue);

        GameObject op = new GameObject(name + WAYPOINT_NAME + (wayPoints.Count + 1).ToString());
        WayPoint wp = op.AddComponent<WayPoint>();
        op.transform.SetParent(transform);
        if (currentWp != null)
            InitCurrentWayPoint(wp, isLinkedToFirst);
        else
            InitFirstWayPoint(wp);
        wp.Init(gameObject, position, transform.rotation, curveToNext);
        wayPoints.Add(wp);
    }

    void InitCurrentWayPoint(WayPoint wp, bool isLinkedToFirst)
    {
        if (isLinkedToFirst)
        {
            wp.next = wayPoints[0];
            wayPoints[0].previous = wp;
        }
        else
            wayPoints[0].previous = null;
        currentWp.next = wp;
        wp.previous = currentWp;
        currentWp = wp;
    }
    
    bool IsCycle(WayPoint start, WayPoint current)
    {
        if (current.next)
        {
            if (start == current.next)
                return true;
            else
                return IsCycle(start, current.next);
        }
        else
            return false;
    }

    void InitFirstWayPoint(WayPoint wp)
    {
        currentWp = wp;
        current = currentWp;
        startPoint = wp;
    }
}
#if UNITY_EDITOR
[CustomEditor(typeof(Path))]
[CanEditMultipleObjects]
public class PathInEditor : Editor
{
    public Path path;
    private void OnEnable()
    {
        EditorApplication.hierarchyChanged += OnHierarchyChange;
    }

    private void OnDisable()
    {
        EditorApplication.hierarchyChanged -= OnHierarchyChange;
    }
         
    private void OnHierarchyChange()
    {
        if (path != null && !Application.isPlaying)
            FilterCurrentPath();
    }

    void FilterCurrentPath()
    {
        if (path != null && path.wayPoints.Count > 0)
        {
            path.wayPoints.RemoveAll(Object => Object == null);
            for (int i = 0; i < path.wayPoints.Count; i++)
            {
                if (IsNotFirst(i) && HasPreviousChange(path.wayPoints[i], path.wayPoints[i - 1]))
                    path.wayPoints[i].previous = path.wayPoints[i - 1];

                if (IsNotLast(i) && HasNextChange(path.wayPoints[i], path.wayPoints[i + 1]))
                    path.wayPoints[i].next = path.wayPoints[i + 1];
            }
        }
    }

    bool IsNotFirst(int i)
    {
        return i != 0 && i - 1 > 0;
    }

    bool IsNotLast(int i)
    {
        return i != path.wayPoints.Count - 1 && path.wayPoints.Count > i + 1;
    }

    bool HasPreviousChange(WayPoint current, WayPoint previous)
    {
        return current.previous != previous;
    }

    bool HasNextChange(WayPoint current, WayPoint next)
    {
        return current.next != next;
    }
      
}
#endif

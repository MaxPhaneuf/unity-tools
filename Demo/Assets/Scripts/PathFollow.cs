﻿using UnityEngine;
using UnityEngine.Events;
/// <summary>
/// Add this to gameObject to make target follow a path created with PathEditor tool.
/// </summary>
[RequireComponent(typeof(Lerpable))]
public class PathFollow : MonoBehaviour
{
    public const int MAX_WAYPOINTS = 300;

    [Tooltip("Path to follow")]
    public Path path;
    [Tooltip("Starting waypoint")]
    [Range(1, MAX_WAYPOINTS)] public int startPoint = 1;
    [Tooltip("Target that follows the path")]
    [SerializeField] Transform target;
    [Tooltip("Target is following path on application start")]
    [SerializeField] bool startActive = true;
    [Tooltip("Target rotates towards the direction of the next frame")]
    [SerializeField] bool looksAtDirection;

    [HideInInspector]
    public UnityEvent Activate = new UnityEvent();
    [HideInInspector]
    public UnityEvent Deactivate = new UnityEvent();
    [HideInInspector]
    public bool isBezierLerp;
    [HideInInspector]
    public WayPoint current, next;

    bool forward;
    Lerpable lerpable;
    Vector3 start, end;
    Vector3 bezierStartPoint, bezierEndPoint;
    Vector3 targetDirection, nextPos;
    GameObject currentTarget;
    bool active;


    private void Start()
    {
        if (target)
            SetTarget();
        else if(transform.GetComponent<Path>() == null)
            target = transform;
        if (path != null && target)
        {
            if (startPoint > path.wayPoints.Count)
                startPoint = path.wayPoints.Count;
            if (path.wayPoints[startPoint - 1])
                Init();
            if (startActive)
                active = true;
        }
        Activate.AddListener(() => SetActive(true));
        Deactivate.AddListener(() => SetActive(false));
    }

    void Update()
    {
        UpdateTarget();
        if (path != null && lerpable != null && target && active)
            Lerp();
    }

    void UpdateTarget()
    {
        if (target && currentTarget != target.gameObject)
            SetTarget();
    }

    /// <summary>
    /// Activate or deactivate follow.
    /// </summary>
    public void SetActive(bool isActive)
    {
        active = isActive;
    }

    /// <summary>
    /// Direction of target following path for this frame. 
    /// </summary>
    public Vector3 GetTargetDirection()
    {
        return targetDirection;
    }

    /// <summary>
    /// True if target following path is moving for this frame.
    /// </summary>
    public bool IsTargetMoving()
    {
        return lerpable.IsLerping();
    }

    /// <summary>
    /// True if target following path is turning for this frame.
    /// </summary>
    public bool IsTargetTurning()
    {
        return isBezierLerp;
    }

    void Lerp()
    {
        if (isBezierLerp)
            LerpBezier();
        else
            LerpDefault();
    }

    #region Target & Direction
    void Init()
    {
        lerpable = GetComponent<Lerpable>();
        current = path.wayPoints[startPoint - 1];
        InitDirection();
        InitDefaultLerp();
    }

    void SetTarget()
    {
        bool targetIsPath = target.GetComponent<Path>() != null;
        if (!targetIsPath)
            currentTarget = target.gameObject;
        else
            ClearTarget();
    }

    void ClearTarget()
    {
        target = null;
        currentTarget = null;
    }

    void InitDirection()
    {
        if (current.next == null)
        {
            if (current.previous)
                next = current.previous;
            forward = false;
        }
        else
        {
            next = current.next;
            forward = true;
        }
    }

    void UpdateTargetPosition(Vector3 nextPosition)
    {
        if(looksAtDirection)
            target.transform.LookAt(nextPosition);
        target.transform.position = nextPosition;
        targetDirection = target.transform.forward;
    }

    void GoToNext()
    {
        current = next;
        if (forward)
            GoForward();
        else
            GoBackward();
    }

    void GoForward()
    {
        if (current.next == null && path.isLooping)
        {
            forward = false;
            next = current.previous;
        }
        else if (current.next != null)
            next = current.next;
    }

    void GoBackward()
    {
        if (current.previous == null && path.isLooping)
        {
            forward = true;
            next = current.next;
        }
        else if (current.previous != null)
            next = current.previous;
    }
    #endregion

    #region Bezier lerp
    void InitBezierLerp()
    {
        lerpable.StopLerp();
        isBezierLerp = true;
        Mathl.LerpCurve lc = new Mathl.LerpCurve(next.bezierTurn.lerpCurve, next.bezierTurn.bezierDuration);
        lerpable.Init(lc);
        lerpable.StartLerp();
        InitBezierPositions();
    }

    void InitBezierPositions()
    {
        bezierStartPoint = Vector3.Lerp(current.tr.position, next.tr.position, next.bezierTurn.bezierStartTime);
        if (forward && next.next)
            bezierEndPoint = Vector3.Lerp(next.tr.position, next.next.tr.position, (1 - next.bezierTurn.bezierStartTime));
        else if (next.previous)
            bezierEndPoint = Vector3.Lerp(next.tr.position, next.previous.tr.position, (1 - next.bezierTurn.bezierStartTime));
        else
            bezierEndPoint = next.tr.position;
    }

    void LerpBezier()
    {
        if (!lerpable.IsLerping())
            EndBezierLerp();
        else if (target)
            UpdateTargetPosition(lerpable.Lerp(bezierStartPoint, next.tr.position, bezierEndPoint));
    }

    void EndBezierLerp()
    {
        GoToNext();
        InitDefaultLerp();
        isBezierLerp = false;
    }
    #endregion

    #region Default lerp
    void InitDefaultLerp()
    {
        lerpable.Init(current.lerpToNext);
        lerpable.StartLerp();
        start = (current.bezierTurn.isBezier ? Vector3.Lerp(current.tr.position, next.tr.position, (1 - current.bezierTurn.bezierStartTime)) : current.tr.position);
        end = (next.bezierTurn.isBezier ? Vector3.Lerp(current.tr.position, next.tr.position, next.bezierTurn.bezierStartTime) : next.tr.position);
    }

    void LerpDefault()
    {
        if (!lerpable.IsLerping() && !next.bezierTurn.isBezier)
            EndDefaultLerp();
        else if (!lerpable.IsLerping() && next != null && next.bezierTurn.isBezier)
            InitBezierLerp();
        else if (target)
            UpdateTargetPosition(lerpable.Lerp(start, end));
    }

    void EndDefaultLerp()
    {
        GoToNext();
        InitDefaultLerp();
    }
    #endregion
}



﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
public class PathSync : MonoBehaviour
{
    [SerializeField] [Range(Mathl.MIN_DURATION, Mathl.MAX_DURATION)] float totalDuration = 1;
    [SerializeField] bool isSyncing;

    void Awake()
    {
        if (isSyncing)
        {
            Transform tr = transform;
            foreach (Transform child in tr)
            {
                Path p = child.GetComponent<Path>();
                if (p)
                {
                    p.totalDuration = totalDuration;
                    p.useTotalDuration = true;
                }
            }
        }
    }
}


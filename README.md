# Unity Tools

## Description

Petits outils pour aider à la production de jeux avec unity:

- LerpTool : Pour ajouter de l'interpolation linéaire à vos objects.
- PathTool : Pour créer des chemin à suivre et les assigner à différent objects.

## Auteurs

Maxime Phaneuf pour le Campus ADN

## Fonctionnement

### LerpTool

- Attachez la composante ```Lerpable``` sur l'objet qui pourra interpoler linéairement sur la courbe choisie.
- Allez chercher votre référence avec ```MonoBehavior.GetComponent<Lerpable>()```.
- Avec la fonction ```Lerpable.Init(Mathl.LerpCurve lerp)``` initialisez votre object ```Lerpable``` avec la ```Mathl.LerpCurve``` de votre choix.
- ```Lerpable.StartLerp()``` démarre l'interpolation, elle s'arrête automatiquement après la durée indiquée par la ```Mathl.LerpCurve```.
- ```Lerpable.StopLerp()``` vous permet d'arrêter l'interpolation quand vous voulez.
- ```Lerpable.IsLerping()``` vous indique si l'interpolation est en cours.

#### Lerpable.Lerp(...)

Il y a présentement 4 surcharges de la fonction ```Lerpable.Lerp(...)``` d'implémentées 
- ```Lerpable.Lerp(Vector3 start, Vector3 end)``` entre les 2 Vector3 pris en argument.
- ```Lerpable.Lerp(Color start, Color end)``` entre les 2 Color pris en argument.
- ```Lerpable.Lerp(float start, float end)``` entre les 2 float pris en argument.
- ```Lerpable.Lerp(Vector3 start, Vector3 mid, Vector3 end)``` entre 3 Vector3 pris en argument pour un point sur un bézier quadratique.

### PathTool

- Dans l'onglet Tools de unity se trouve le PathEditor.
- Vous pouvez créer un chemin de point (```WayPoint```) avec celui-ci.
- Attachez la composante ```PathFollow``` sur un objet dans la scène.
- Glissez la référence du chemin à suivre dans le champ ```Path``` de la composante ```PathFollow```.
- Glissez la référence de l'object qui devra suivre le chemin dans le champ ```Target``` de la composante ```PathFollow```.
- Personalisez avec les options implémentées.
- ```PathFollow.SetActive(bool isActive)``` vous permet d'activer ou désactiver la poursuite.
- ```PathFollow.GetTargetDirection()``` retourne la direction de la cible du chemin pour ce frame.
- ```PathFollow.IsTargetMoving()``` retourne vrai si la cible du chemin est en mouvement ce frame.
- ```PathFollow.IsTargetTurning()``` retourne vrai si la cible du chemin tourne sur un bézier ce frame. 

## Dépendances
 
- [Progrids](https://assetstore.unity.com/packages/tools/modeling/progrids-2-x-111425)
